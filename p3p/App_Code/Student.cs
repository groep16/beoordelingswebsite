﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Summary description for Student
/// </summary>
public class Student
{
    public int studentId { get; set; }
    public int studentNumber { get; set; }
    public string studentName { get; set; }

    private List<EvaluationForm> _evaluationForms; 
    public List<EvaluationForm> EvaluationForms
    {
        get
        {
            if (_evaluationForms == null)
            {
                _evaluationForms = EvaluationFormRepository.SelectEvaluationFormsByStudentId(studentId);
            }
            return _evaluationForms;
        }
        set { _evaluationForms = value; }
    }

    public Student(int studentId, int studentNumber, string studentName)
    {
        this.studentId = studentId;
        this.studentNumber = studentNumber;
        this.studentName = studentName;
    }
    public Student(int studentNumber,string studentName)
    {
        
        this.studentName = studentName;
        this.studentNumber = studentNumber;    
    }
}