﻿using System;
using System.Collections.Generic;
using System.Web;

public class Field
{
    public string label,field_type,required;
    public Field_options field_options;

    public string toHtml(string value = ""){
        string html = "";
        html+="<tr><td><h1>" + label + "</h1></td></tr>";
        html+= HtmlForm.GetFieldByType(this,value);
        return html;
    }
}
