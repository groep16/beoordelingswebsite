﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Helpers;
using System.Web.WebPages;
using System.Web.WebPages.Html;
using WebMatrix.Data;

public class EvaluationForm
{
    public int      formId      { get; set; }
    public string   title       { get; set; }
    public DateTime dateOf      { get; set; }
    public string   commentary  { get; set; }
    public int      studentId   { get; set; }

    private Student _student;
    public Student Student
    {
        get
        {
            if (_student == null)
            {
                _student = StudentRepository.SelectById(studentId);
            }
            return _student;
        }
        set { _student = value; }
    }

    public EvaluationForm() {}

    public EvaluationForm(int formId, int studentId, string title, string commentary, DateTime dateOf)
    {
        this.formId = formId;
        this.studentId = studentId;
        this.title = title;
        this.commentary = commentary;
        this.dateOf = dateOf;
    }
}


