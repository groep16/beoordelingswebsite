﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for HtmlForm
/// </summary>
public static class HtmlForm
{
    public static string GetFieldByType(Field field,string value=""){
        string html = "";
        switch(field.field_type){
            case "dropdown":
                html = dropdown(field,value);
                break;
            case "radio":
                html = radio(field,value);
                break;
            case "checkboxes":
                html = checkbox(field,value);
                break;
            case "paragraph":
                html = textarea(field,value);
                break;
            case "text":
                html = input(field,value);
                break;
        }

        return html;
    }

    public static string input(Field field,string value=""){
        string html = "<tr><td><input type='text' name='"+ field.label +"' value='"+value+"' class='rf-size-" + field.field_options.size + " '></td></tr>";
        html+= "<tr><td>" + field.field_options.description + "</td></tr>";
        return html;
    }

    public static string textarea(Field field,string value=""){
        string html = "<tr><td><textarea name='"+ field.label +"' class='rf-size-" + field.field_options.size + "'>"+value+"</textarea></td></tr>";
        html+= "<tr><td>" + field.field_options.description + "</td></tr>";
        return html;
    }

    public static string checkbox(Field field,string value=""){
        string html = "";
        foreach(var item in field.field_options.options){
            string[] values = value.Split(',');
            var checke = "";
            html+="<tr><td>" + item.label + "</td></tr>";
            if(value != "" && Array.IndexOf(values,item.label) > -1 && values[Array.IndexOf(values,item.label)] == item.label){
                checke="checked";
            }

            html+= "<tr><td><input name='"+ field.label +"[]' type='checkbox' "+checke+" value='"+item.label+"'> " + item.label + "</td></tr>";    
        }
        html+= "<tr><td>" + field.field_options.description + "</td></tr>";

        return html;
    }

    public static string radio(Field field,string value=""){
        string html = "";
        foreach(var item in field.field_options.options){
            var check = "";
            if(value==item.label){
                check="checked";
            }

            html+= "<tr><td><input type='radio' name='" + field.label + "[]' "+check+" value='"+item.label+"'> "+item.label+"</td></tr>";    
        }
        html+= "<tr><td>" + field.field_options.description + "</td></tr>";

        return html;
    }

    public static string dropdown(Field field,string value=""){
        string html = "";
        if(field.field_options.include_blank_option !=null){
                html+="<option></option>";
        }

        foreach(var item in field.field_options.options){


            string selected = "";
            if(value == item.label){
                selected = "selected";
            }

            html+= "<option "+selected+">" + item.label + "</option>";    
        }
        html+= "</select></td></tr>";
        html+= "<tr><td>" + field.field_options.description + "</td></tr>";
        html="<tr><td><select name='"+ field.label +"'>" +html;

        return html;
    }
}
