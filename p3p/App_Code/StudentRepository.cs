﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Summary description for StudentRepository
/// </summary>
public class StudentRepository
{
    public static List<Student> SelectAll()
    {
        List<Student> result = new List<Student>();

        Database db = Database.Open("BeoordelingsFormulieren");
        var records = db.Query("SELECT studentId, studentNumber, studentName FROM Student");
        foreach (var record in records)
        {
            result.Add(new Student(record.studentId, record.studentNumber, record.studentName));
        }
        db.Close();

        return result;
    }

    public static Student SelectById(int studentId)
    {
        Database db = Database.Open("BeoordelingsFormulieren");
        var catRecord = db.QuerySingle("SELECT studentId, studentNumber, studentName FROM Student WHERE studentId = @0", studentId);
        Student result = new Student(catRecord.studentId, catRecord.studentNumber, catRecord.studentName);
        db.Close();
        return result;
    }

    public static Student Insert(int studentNumber, string studentName)
    {
        Database db = Database.Open("BeoordelingsFormulieren");
        int numRows = db.Execute("INSERT INTO Student (studentNumber, studentName) VALUES (@0, @1)", studentNumber, studentName);
        if (numRows <= 0)
            return null;

        Student result = new Student(studentNumber, studentName);
        db.Close();
        return result;
    }

    public static bool Delete(int studentId)
    {
        Database db = Database.Open("BeoordelingsFormulieren");
        int numRows = db.Execute("DELETE FROM Student (studentNumber, studentName) WHERE studentId = @0", studentId);
        return numRows == 1;
    }
}