﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.WebPages;
using WebMatrix.Data;

/// <summary>
/// Summary description for EvaluationFormRepository
/// </summary>
public class EvaluationFormRepository
{
    public static List<EvaluationForm> SelectAll()
    {
        List<EvaluationForm> evaluationForms = new List<EvaluationForm>();

        Database db = Database.Open("BeoordelingsFormulieren");
        var rows = db.Query("SELECT formId, title, dateOf, commentary, studentId FROM EvaluationForm");
        foreach (var row in rows)
        {
            evaluationForms.Add(new EvaluationForm(row.formId, row.studentId, row.title, row.commentary, row.dateOf));
        }

        db.Close();

        return evaluationForms;
    } 

    public static EvaluationForm SelectEvaluationFormsByTitle(string title)
    {
        Database db = Database.Open("BeoordelingsFormulieren");
        var row = db.QuerySingle("SELECT formId, title, dateOf, commentary, studentId FROM EvaluationForm WHERE title LIKE %@0%",
            title);

        if (row == null)
        {
            db.Close();
            return null;
        }

        EvaluationForm p = new EvaluationForm(row.formId, row.studentId, row.title, row.commentary, row.dateOf);
        db.Close();
        return p;
    }

    public static EvaluationForm SelectEvaluationFormsById(int formId)
    {
        Database db = Database.Open("BeoordelingsFormulieren");
        var row = db.QuerySingle("SELECT formId, title, dateOf, commentary, studentId FROM EvaluationForm WHERE formId = @0",
            formId);

        if (row == null)
        {
            db.Close();
            return null;
        }

        EvaluationForm p = new EvaluationForm(row.formId, row.studentId, row.title, row.commentary, row.dateOf);
        db.Close();
        return p;
    }

    public static List<EvaluationForm> SelectEvaluationFormsByStudentId(int studentId)
    {
        List<EvaluationForm> result = new List<EvaluationForm>();

        Database db = Database.Open("BeoordelingsFormulieren");
        var rows = db.Query("SELECT formId, studentId, title, commentary, dateOf FROM EvaluationForm WHERE studentId = @0", studentId);
        foreach (var row in rows)
        {
            result.Add(new EvaluationForm(row.formId, row.studentId, row.title, row.commentary, row.dateOf));
        }
        db.Close();

        return result;
    }

    public static int Insert(EvaluationForm evaluationForm)
    {
        Database db = Database.Open("BeoordelingsFormulieren");
        int numRowsEffected = db.Execute("INSERT INTO EvaluationForm (title, dateOf, commentary) VALUES (@0, @1, @2)",
            evaluationForm.title, evaluationForm.dateOf, evaluationForm.commentary);

        int result = -1;
        if (numRowsEffected == 1)
        {
            result = Convert.ToInt32(db.GetLastInsertId());
        }

        db.Close();

        return result;
    }

    public static bool Delete(int formId)
    {
        Database db = Database.Open("BeoordelingsFormulieren");
        int numRowsEffected = db.Execute("DELETE FROM EvaluationForm WHERE formId = @0",
            formId);
        db.Close();
        return numRowsEffected == 1;
    }

    public static int InsertByForm(WebPage page)
    {
        if (page.IsPost)
        {
            page.Validation.RequireField("title", "title mag niet leeg zijn");
            page.Validation.Add("title", Validator.StringLength(
                maxLength: 100, minLength: 3,
                errorMessage: "title moet tussen 3 en 100 tekens bevatten"));

            page.Validation.RequireField("dateOf", "dateOf mag niet leeg zijn");
            page.Validation.Add("dateOf", Validator.DateTime("dateOf moet een datum zijn"));

            page.Validation.RequireField("commentary", "commentary moet geleverd zijn");

            if (page.Validation.IsValid())
            {
                string title = page.Request.Form["title"];
                DateTime dateOf = page.Request.Form["dateOf"].AsDateTime();
                string commentary = page.Request.Form["commentary"];

                EvaluationForm evaluationForm = new EvaluationForm() { };
                evaluationForm.title = title;
                evaluationForm.dateOf = dateOf;
                evaluationForm.commentary = commentary;
                return EvaluationFormRepository.Insert(evaluationForm);
            }
            else
            {
                return -1;
            }
        }
        return -1;
    }
}