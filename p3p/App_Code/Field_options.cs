﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Field_options
/// </summary>
public class Field_options
{
    public string size;
    public string label;
    public string description;
    public string include_blank_option;
    public List<Field_options> options;
}
